import React from "react";
import styled from "@emotion/styled";

const Wrapper = styled.img`
  display: block;
  border-radius: 10px;
`;

export default function Image({ src }) {
  return <Wrapper src={src} />;
}