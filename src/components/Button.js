import React from "react";
import styled from "@emotion/styled";
import { button } from "styles/Button";

const Wrapper = styled.button`
  ${button};
`;

export default function Button({ text }) {
  return <Wrapper>{text}</Wrapper>;
}