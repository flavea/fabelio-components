import { css } from "@emotion/core";

export const button = css`
  background: #fed541;
  border-radius: 3px;
  color: #000;
  padding: .5em 1em;
  font-size: 1em;
  font-weight: bold;
  border: none;
`;
