# Fabelio3 React Components

Design components for Fabelio3. (Documentation: Coming soon)

Created with:
- **[React](https://reactjs.org/), [Emotion](https://emotion.sh/docs/introduction)** for creating and styling components,
- **[Jest](https://jestjs.io/)**, **[react-testing-library](https://github.com/testing-library/react-testing-library)** for testing the components,
- **Rollup**, **Babel** for bundling the library,
- **[Styleguidist](https://react-styleguidist.js.org/)** for live development and documentation generation,

## For Development
- Clone this repository
- Run `yarn install`
- For live development run `yarn start` and open the localhost, we can see how the component looks like there.

## How to use in other projects
In your project, add this package by running
```yarn add https://flavea@bitbucket.org/fabeliocoders/fabelio3-components.git```